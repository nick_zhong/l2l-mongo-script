const m = require('../tool/mongo');

async function main() {
  console.time('elapsed_time');
  console.log(' ...start execute script');
  const accountCol = await m.collection('Account');
  const taskCol = await m.collection('Task');
  const opportunityCol = await m.collection('Opportunity');
  console.log(' ...get collection client');

  const [accs, tasks, opps] = await Promise.all(
    [
      accountCol.find().toArray(),
      taskCol.find().toArray(),
      opportunityCol.find().toArray(),
    ]
  );
  console.log(' ...fetch data done');

  const accIds = accs.map(v=>v._id);
  const taskAccIdMap = {};
  const oppAccIdMap = {};
  tasks.forEach(t=> taskAccIdMap[t.accountId]=1);
  opps.forEach(o=> oppAccIdMap[o.accountId]=1);

  const worongAccIdsOfTask = [];
  const worongAccIdsOfOpp = [];

  const taskAccIds = Object.keys(taskAccIdMap);
  const oppAccIds = Object.keys(oppAccIdMap);

  taskAccIds.forEach(accId=>{
    if(!accIds.includes(accId))worongAccIdsOfTask.push(accId);
  });
  oppAccIds.forEach(accId=>{
    if(!accIds.includes(accId))worongAccIdsOfOpp.push(accId);
  });

  console.log(worongAccIdsOfTask.join());
  console.log('------------');
  console.log(worongAccIdsOfOpp.join());

  console.log(`start to delete task`);
  await taskCol.remove({accountId:{'$in':worongAccIdsOfTask}});
  console.log(`delete task done`);

  console.log(`start to delete opportunity`);
  await opportunityCol.remove({accountId:{'$in':worongAccIdsOfOpp}});
  console.log(`delete opportunity done\n`);

  console.log(' ... finish');
  console.timeEnd('elapsed_time');
}

main().catch(err => console.log(err));

