const m = require('../tool/mongo');

async function main() {
  const col = await m.collection('Task');
  const count = await col.count();
  console.log(count);
}

main().catch(err => console.log(err));

