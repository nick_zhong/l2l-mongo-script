/**
 * mongo client for mongo4.0
 */

var MongoClient = require('mongodb').MongoClient;
var Server = require('mongodb').Server;
var mongoConfig = require('../config/mongo');

var db_clientIns_ = {};

function getClientInstance(configKey) {
  return new Promise((resolve, reject) => {
    var clientIns = db_clientIns_[configKey];
    if (clientIns) {
      resolve(clientIns);
    } else {
      var conf = mongoConfig[configKey];
      if (!conf) return reject(new Error(`no configured options for configKey ${configKey}`));
      var { ip, port, user, password } = conf;
      new MongoClient(new Server(ip, port), { user, password, authSource: 'admin' }).connect((err, client) => {
        db_clientIns_[configKey] = client;
        err ? reject(err) : resolve(client);
      });
    }
  });
}

exports.collection = async function (collectionName, dbName = 'data-boldseas') {
  const client = await getClientInstance('l2l');
  return client.db(dbName).collection(collectionName);
}